package kz.aitu.week9;

public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph<Integer, String>();
        graph.addVertex(3,"abcd");
        graph.addVertex(21,"opqr");
        graph.addVertex(14,"stuv");
        graph.addVertex(7,"vwx");
        graph.addVertex(8,"qwerty");
        graph.addVertex(2,"ytrewq");
        graph.addEdge(3,2,false);
        graph.addEdge(21,3,false);
        graph.addEdge(14,2,true);
        graph.addEdge(21,8,true);
        graph.addEdge(7,3,false);
        graph.addEdge(3,14,true);
        graph.connectedWith(21);
        graph.connectedWith(8);
        graph.connectedWith(7);
        graph.countVertex();
        graph.printAll();
        graph.addVertex(11,"vwer");
        graph.addVertex(12,"qgtey");
        graph.countVertex();
    }

}
