package kz.aitu.project_w3quiz;
public class LinkedList {
    private Node head;
    private Node tail;

    public LinkedList() {
        this.head = new Node("head");
        tail = head;
    }

    public Node head() {
        return head;
    }

    public void add(Node node) {
        tail.next = node;
        tail = node;
    }

    public void insertAt(String s, int index) {
        //write your code here
        Node n = new Node(s);
        Node a = head;
        int i = 1;
        while (i < index - 1) {
            a = a.next;
            i++;
        }
        Node current = a.next;
        n.next = current;
        a.next = n;
    }
    public void removeAt(int index) {
        //write your code here
        Node a = head;
        int i = 1;
        while (i < index - 1) {
            a = a.next;
            i++;
        }
        Node current = a.next;
        a.next = current.next;
        current.next = null;
    }
}
