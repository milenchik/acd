package kz.aitu.week6.HashTable;

public class HashTable {
    Node[] hashTable;
    int size;

    public HashTable(int length) {
        size = length;
        hashTable = new Node[length];
    }

    public boolean isEmpty() {
        if (size == 0) return true;
        else return false;
    }

    public boolean isFull() {
        if (size == hashTable.length) return true;
        else return false;
    }
    public void insert(String key, String data){
        Node newNode = new Node(key, data);
      int i=key.hashCode()%size;
      Node currentInd=hashTable[i];
      if(currentInd==null){
          hashTable[i]=newNode;
      }
      while(currentInd!=null){
          if(currentInd.getKey()==key){
              currentInd.setData(data);
          }
          else if (currentInd.getNext()==null){
              currentInd.setNext(newNode);
          }
          currentInd=currentInd.getNext();
      }
      size++;
    }
    public void delete(String key){
        int i=key.hashCode()%size;
        Node currentInd=hashTable[i];
        if(currentInd==null){return;}
        if(currentInd.getKey()==key){
            currentInd=currentInd.getNext();
        }
        while(currentInd.getNext()!=null){
            if(currentInd.getNext().getKey()==key){
                currentInd.setNext(currentInd.getNext().getNext());
            size--;
                return;
            }
        }

    }
    public int size() {
        return size;
    }

    public String printData(String key) {
        int i = key.hashCode()% size;
        Node currentInd = hashTable[i];
        while (currentInd!= null) {
            if (currentInd.getKey() == key) {
                return currentInd.getData();
            }
            currentInd = currentInd.getNext();
        }
        return null;
    }



    public void printAll(){
        for(int i=0;i<hashTable.length;i++){
            System.out.println();
        }
    }

}
