package kz.aitu.week6.HashTable;

public class Node {
    private String key;
    private String data;
    private Node next;

    public Node(String key, String data) {
        this.key=key;
        this.data=data;
        this.next=null;
    }


    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
