package kz.aitu.sort;

public class Merge {

    public void sort(int[] arr) {
     int i;
     int j;
     int temp;
     int middle;
     if(arr.length<=1) return;
     middle=arr.length/2;
     int [] leftArr = new int [middle];
     int [] rightArr;
    if(arr.length%2==0){
     rightArr = new int[middle];
    }
    else{rightArr= new int[middle+1];}
    for(i=0;i<middle; i++){
     leftArr[i]=arr[i];
    }
    for(j=0; j<rightArr.length; j++){
     rightArr[j]=arr[middle+j];
    }
    int[] finalArr=new int[arr.length];
   // leftArr=sort(leftArr);
    //rightArr=sort(rightArr);
    finalArr=mergeS(leftArr, rightArr);
    return;
    }
    private static int[] mergeS(int[] leftArr, int[] rightArr){
     int currentLeft=0;
     int currentRight=0;
     int currentFinal=0;
     int [] finalArr=new int [leftArr.length + rightArr.length];

     // While there are items in either array...
     while(currentLeft < leftArr.length || currentRight < rightArr.length) {

      // If there are items in BOTH arrays...
      if(currentLeft < leftArr.length && currentRight < rightArr.length) {

       // If left item is less than right item...
       if(leftArr[currentLeft] < rightArr[currentRight]) {

        finalArr[currentFinal++] = leftArr[currentLeft++];

       } else {

        finalArr[currentFinal++] = rightArr[currentRight++];

       }

      }

      // If there are only items in the left array...
      else if(currentLeft < leftArr.length) {

          finalArr[currentFinal++] = leftArr[currentLeft++];

      }

      // If there are only items in the right array...
      else if(currentRight < rightArr.length) {

          finalArr[currentFinal++] = rightArr[currentRight++];

      }

     }

     return finalArr;

    }
}
