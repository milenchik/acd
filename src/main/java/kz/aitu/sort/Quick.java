package kz.aitu.sort;

public class Quick {

    public void sort(int[] arr, int low, int high) {
int middle= arr.length/2;
low=0;
high=arr.length;
int i=low;
int j=high;
int pivot=arr[middle];
while(i<=j){
    while(arr[i]<pivot){
        i++;
    }
    while(arr[j]>pivot){j--;}
    if(i<=j){
        int temp=arr[i];
        arr[i]=arr[j];
        arr[j]=temp;
        i++;
        j--;
    }
   if(low<j){
       sort(arr, low, high);
   }
   if(high>i){sort(arr, low, high);}
}
    }
}
