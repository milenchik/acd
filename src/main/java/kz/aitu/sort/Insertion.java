package kz.aitu.sort;

public class Insertion {

    public void sort(int[] arr) {
     int i;
     int j;
     int temp;
     for(i=1;i<arr.length; i++){
         temp=arr[i];
         j=i-1;
         while(j>=0 && temp<arr[j]){
             arr[j+1]=arr[j];
             j=j-1;
         }
         arr[j+1]=temp;
     }
    }
}
