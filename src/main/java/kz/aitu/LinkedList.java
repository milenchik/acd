package kz.aitu;
import java.util.Scanner;
public class LinkedList {
private Block first;
private Block last;
    public void addBlock(Block block){
if(first==null){
    first=block;
    last=block;
}else{last.setNextBlock(block);
last=block;}
    }

    public Block getFirst() {
        return first;
    }

    public void setFirst(Block first) {
        this.first = first;
    }

    public Block getLast() {
        return last;
    }

    public void setLast(Block last) {
        this.last = last;
    }
}
