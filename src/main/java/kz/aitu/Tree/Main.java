package kz.aitu.Tree;

public class Main {
    public static void main(String[] args) {
       // BSTree bsTree = new BSTree();

      /*  bsTree.insert(1000, "A");
        bsTree.insert(2000, "B");
        bsTree.insert(500, "C");
        bsTree.insert(1500, "D");
        bsTree.insert(750, "E");
        bsTree.insert(250, "F");
        bsTree.insert(625, "G");
        bsTree.insert(1250, "H");
        bsTree.insert(875, "I");
        bsTree.insert(810, "Z");

        System.out.println("===Print 810 (Z)");
        System.out.println(bsTree.find(810));
        System.out.println("===Print ALLAscending(FCGEZIAHDB)");
        bsTree.printAllAscending();
        System.out.println("===Print ALL(ACBFEDGIHZ)");
        bsTree.printAll();
        System.out.println("===Removing 1000");
        System.out.println("===Print ALLAscending(FCGEZIHDB)");
        bsTree.delete(1000);
        bsTree.printAllAscending();
        System.out.println("===Print ALL(ICBFEDGZH)");
        bsTree.printAll();


        /*bsTree.insert(5, "A");
        bsTree.insert(20, "B");
        bsTree.insert(10, "C");


        bsTree.delete(20);
        bsTree.printAll();*/
        public static void bfs(BSTree t){
            if (t.getRoot() == null) {
                return;
            }
            Queue q = new Queue();
            q.add(t.getRoot().getData());
            while(!q.isEmpty()) {
                Node temp = q.poll();
                System.out.print(temp.getData() + " ");
                BinaryTreeNode b = t.find(temp.getData());
                if (b.getLeft() != null) {
                    q.add(b.getLeft().getData());
                }
                if (b.getRight() != null) {
                    q.add(b.getRight().getData());
                }
            }
        }

       /* public static void main(String[] args) {
            BSTree t = new BSTree();
            t.insert(1);
            t.insert(2);
            t.insert(3);
            t.insert(4);
            t.insert(5);

            bfs(t);*/
        }

    }

