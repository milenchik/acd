package kz.aitu.Tree;

public class Node {
    private int data;
    private kz.aitu.week4.queue.Node nextNode;

    public int getNodeValue() {
        return data;
    }

    public void setNodeValue(int data) {
        this.data = data;
    }

    public kz.aitu.week4.queue.Node getNextNode() {
        return nextNode;
    }

    public void setNextNode(kz.aitu.week4.queue.Node nextNode) {
        this.nextNode = nextNode;
    }
}
