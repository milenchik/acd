 package kz.aitu.Tree;

public class BSTree {
   /* private BSTreeNode root;

    public void setRoot(BSTreeNode root) {
        this.root = root;
    }
    public BSTreeNode getRoot() {
        return root;
    }
    public void insert(int a) {
        BSTreeNode node = new BSTreeNode;
        if (root == null) {
            root = node;
        } else {
            BSTreeNode current=root;
            BSTreeNode parent=null;
            parent=current;
            if(a< current.value){
                current=current.left;
                if(current==null){parent.left=node; return;}
            }
         else{
            current=current.right;
            if(current==null){parent.right=node; return;}
        }
    }
        public void display(BSTreeNode root) {
            if (root != null) {
                display(root.left);
                System.out.print(" " + root.data);
                display(root.right);
            }
        }
        public BSTreeNode find(int data) {
            return findNode(this.root, data);
        }
        private BSTreeNode findNode(BSTreeNode curr, int ser) {
            if (curr == null) {
                return null;
            }
            if (ser > curr.data) {
                return findNode(curr.right, ser);
            } else if (ser < curr.data) {
                return findNode(curr.left, ser);
            } else {
                return curr;
            }
        }



    public boolean delete(Integer key) {
        Node current = root;
        Node previous = null;
        boolean found = false;

        while(!found) {
            if (key > current.getKey()) {
                previous = current;
                current = current.getRight();

                if (current == null) {
                    break;
                }
            } else if(key < current.getKey()) {
                previous = current;
                current = current.getLeft();

                if (current == null) {
                    break;
                }
            } else {
                found = true;
            }
        }

      //  if (!found) {
            return false;
        }

        if (current.getLeft() == null && current.getRight()== null) {
            if (previous.getRight() == current) {
                previous.setRight(null);
            } else {
                previous.setLeft(null);
            }
        } else if (current.getLeft() != null && current.getRight()==null) {
            if (previous.getRight() == current) {
                previous.setRight(current.getLeft());
            } else {
                previous.setLeft(current.getLeft());
            }
        } else if(current.getLeft() == null && current.getRight() != null) {
            if (previous.getRight() == current) {
                previous.setRight(current.getRight());
            } else {
                previous.setLeft(current.getRight());
            }
        } else {
            if (previous.getRight() == current) {
                previous.setRight(current.getRight());
                insert(current.getLeft().getKey(), current.getLeft().getValue(), current.getRight());
            } else {
                previous.setLeft(current.getRight());
                insert(current.getLeft().getKey(), current.getLeft().getValue(), current.getRight());
            }
        }

        return true;
    }

    public String find(Integer key) {
        Node node = findNode(root, key);
        if(node == null) return null;
        else return node.getValue();
    }

    private Node findNode(Node node, Integer key) {
        if(node == null) return null;
        if(key > node.getKey()) return findNode(node.getRight(), key);
        else if(key < node.getKey()) return findNode(node.getLeft(), key);
        else return node;
    }

    public String findWithoutRecursion(Integer key) {
        Node current = root;
        while(current!=null) {
            if(key > current.getKey()) current = current.getRight();
            else if(key < current.getKey()) current = current.getLeft();
            else return current.getValue();
        }
        return null;
    }

    public void printAllAscending() {
        printNodeAsc(root);
        System.out.println();
    }


        public void printAll() {
            int h = BSTreeHeight(root);
            for(int i = 1; i<h+1; i++){
                printALevel(root,i);
            }
        }

        private int BSTreeHeight(Node newNode){
            if(newNode == null){
                return 1;
            }
            else{
                int leftheight = BSTreeHeight(newNode.getLeft());
                int rightheight = BSTreeHeight(newNode.getRight());

                if (leftheight>rightheight){
                    return  (rightheight+1);}
                else {
                    return  (rightheight+1);
                }
            }
        }

        private void printALevel (Node root, int level)
        {
            if (root == null)
                return;
            if (level == 1)
                /* System.out.print(root.getValue() + " ");*/
               /* System.out.println("Key: "+ root.getKey()+ " value: "+ root.getValue());
            else if (level > 1)
            {
                printALevel(root.getLeft(), level-1);
                printALevel(root.getRight(), level-1);
            }
        }*/


    // printAll(root);


    /*private void printAll(Node node) {
        if (node != null) {
            System.out.println(node.getValue());
            if (node.getLeft()!=null) {
                System.out.println("Left children (node " + node.getValue() +"): ");
                printAll(node.getLeft());
            }
            if (node.getRight()!=null) {
                System.out.println("Right children (node " + node.getValue() +"): ");
                printAll(node.getRight());
            }
        }
    }*/

   /* private void printNode(Node left, Node right) {
        if(left != null) System.out.print(left.getKey() + ",");
        if(right != null) System.out.print(right.getKey() + ",");
        if(left != null) printNode(left.getLeft(), left.getRight());
        if(right != null) printNode(right.getLeft(), right.getRight());
    }

    private void printNodeAsc(Node node) {
        if(node == null) return;
        printNodeAsc(node.getLeft());
        System.out.print(node.getValue());
        printNodeAsc(node.getRight());
    }
}*/


}