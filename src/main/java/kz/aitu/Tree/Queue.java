package kz.aitu.Tree;

import kz.aitu.week4.queue.Node;

public class Queue {
    private kz.aitu.week4.queue.Node head;
    private kz.aitu.week4.queue.Node tail;
    private int size;

    public  boolean empty() {
        if (head == null) {
            return true;
        } else return false;
    }
    public void setHead(int nodeValue) {
        kz.aitu.week4.queue.Node n = new kz.aitu.week4.queue.Node();
        n.setNodeValue(nodeValue);
        size++;
    }

    public void add(int nodeValue) {
        kz.aitu.week4.queue.Node n = new kz.aitu.week4.queue.Node();
        n.setNodeValue(nodeValue);
        if(head==null){
            head=n;
            tail=n;
        size++;}
        else{
            tail.setNextNode(n);
            n.setNextNode(null);
            tail = n;
            size++;
        }

    }

    public int poll() {
        kz.aitu.week4.queue.Node oldHead = head;
        if(head==null){return 0;}
        else {
            head = head.getNextNode();
            size--;
            return oldHead.getNodeValue();
        }
    }

    public int peek() {
        Node n = head;
        if (head==null) {
            return 0;}
        else{
            return n.getNodeValue();}
    }

    public int size() {
        return size;
    }

   /* public void printQueue(Node node) {
        while (node.getNextNode() != null) {
            System.out.println(node.getNodeValue());
            node = node.getNextNode();
        }
        System.out.println(node.getNodeValue());
    }*/

}


