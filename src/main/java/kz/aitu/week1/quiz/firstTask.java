package kz.aitu.week1.quiz;
import java.util.Scanner;
public class firstTask {
    public static String rec(int n) {
        if (n == 1) {
            return "1";
        }
        return rec(n - 1) + " " + n;
    }
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int n = input.nextInt();

        System.out.println(rec(n));
    }
}


