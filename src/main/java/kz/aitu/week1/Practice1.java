package kz.aitu.week1;

import java.util.Scanner;
public class Practice1 {
    static int[][] a = new int[5][5];
    public static void inputData() {
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                a[i][j] = scanner.nextInt();
            }
        }
    }

    public static void print() {
        for (int i = 0; i < 5; i ++) {
            for (int j = 0; j < 5; j ++) {
                System.out.print(a[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static boolean findPath(int i, int j) {
        int n=0;
        int m=0;
        int [][] arr= new int[n][m];

        if(arr[i][j]==0) return false;
        if(i==(n-1) && j==(m-1)) {arr[i][j]=2; return true;}
        arr[i][j]=0;
        boolean flag=false;
        if(i+1!=n) flag=flag||findPath(i+1,j);
        if(j+1!=n) flag=flag||findPath(i,j+1);
        if(i!=0) flag=flag||findPath(i-1,j);
        if(j!=0) flag=flag||findPath(i,j-1);
        return flag;
    }

    public static void main(String[] args) {
        inputData();
        System.out.println(findPath(0, 0));
        print();
    }
}
