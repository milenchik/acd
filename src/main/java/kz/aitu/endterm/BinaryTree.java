package kz.aitu.endterm;

public class BinaryTree {
    public void BinaryTree(){
        private Node root;

    public BinaryTree(Node root) {
            this.root = null;
        }
        public void insert (int key, String value) {
            if (root == null) {
                root = new Node(key, value);
            } else {
                insert(key, value, root);
            }
        }

        private void insert (int key, String value, Node node) {
            if (key < node.getKey()) {
                if (node.getLeft() == null) {
                    node.setLeft(new Node(key, value));
                } else {
                    insert(key, value, node.getLeft());
                }
            } else {
                if (node.getRight() == null) {
                    node.setRight(new Node(key, value));
                } else {
                    insert(key, value, node.getRight());
                }
            }
        }


    }
    }


    }
}
